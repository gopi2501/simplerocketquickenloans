import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-user-options',
  templateUrl: './user-options.component.html',
  styleUrls: ['./user-options.component.css']
})
export class UserOptionsComponent implements OnInit {

  orgName: string;
  interestRate: string;
  @Input('userOption') userOption: {orgName: string, interestRate: string};
  constructor() { }

  ngOnInit() {
    this.orgName = this.userOption.orgName;
    this.interestRate = this.userOption.interestRate;
  }

  productHasBeenSelected(){
    console.log('A product has been selected which is from ' + this.orgName + ' and the interest rate is  ' + this.interestRate);
  }

}
