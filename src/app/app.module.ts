import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import {FormsModule} from '@angular/forms';


import { AppComponent } from './app.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UserOptionsComponent } from './user-options/user-options.component';

@NgModule({
  declarations: [
    AppComponent,
    UserFormComponent,
    UserOptionsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
