import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, HttpModule, Response } from '@angular/http';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

private userOptions = new BehaviorSubject<Object>(null);

public userOptionsObservable = this.userOptions.asObservable();
  constructor(private http?: Http) { }


  postJSON(url: string, postBody: any): Observable<any>{
    let headers =  new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({ headers: headers});
    let resp: Response;
    try{
      return this.http.post(url, postBody, options).map((res: Response) => res.json())
        .do(data => {
          return data; })
        .catch( error => {
          return new Observable(null);
        });
      } catch (e) {

      }
  }

  getJSON(url: string): Observable<any> {
    let headers = new Headers({'Content-Type': 'application/json'});
    let myparameters = new URLSearchParams();
    myparameters.append('source', 'ui');
    let options = new RequestOptions({params: {'source': 'ui'}});
    try{
      return this.http.get(url, options).map((res: Response) => res.json())
      .do(data => {
         return data; });
    } catch(e){

    }
  }

   itemsArray = [ {"orgName": "ABC", "interestRate": "30 years at %4.15"}, {"orgName": "DCD", "interestRate": "15 years at %3.85"},
              {"orgName": "NCN", "interestRate": "30 years at %3.89"}, {"orgName": "MIM", "interestRate": "30 years at %4.75"},
              {"orgName": "JACK", "interestRate": "30 years at %5.15"}, {"orgName": "KING", "interestRate": "30 years at %4.05"},
              {"orgName": "TNANCY", "interestRate": "Test Values"} ];

  getUserProductOptions(userDetailForm: NgForm) {
    console.log('hello I am from the users service class');
    this.userOptions.next(this.itemsArray);
  }

}
