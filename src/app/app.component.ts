import { Component } from '@angular/core';
import { UserServiceService } from './user-services/user-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserServiceService]
})
export class AppComponent {
  title = 'TeamRocket';
  lengthOfItems = 7;
  itemsArray: Object = [];
  constructor(public userService?: UserServiceService) {
    this.subscribeTheUserService();
  }


  // itemsArray = [
  //             {"orgName": "FHA Conventional", "interestRate": "30 years at %4.15"},
  //             // {"orgName": "VA Conventional", "interestRate": "15 years at %3.85"},
  //             // {"orgName": "FHA Variable ARM", "interestRate": "30 years at %3.89"},
  //             // {"orgName": "Fannie Mae HELOC", "interestRate": "30 years at %4.75"},
  //             // {"orgName": "FreddieMac Conventional", "interestRate": "30 years at %5.15"},
  //             {"orgName": "VA ARM", "interestRate": "30 years at %4.05"},
  //             {"orgName": "Test Data", "interestRate": "Test Values"}
  //           ];

  subscribeTheUserService() {
    this.userService.userOptionsObservable.subscribe(
      itemsArrayFromUserService => {
        this.itemsArray = itemsArrayFromUserService;
      }
    );
  }

}
