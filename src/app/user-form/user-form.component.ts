import { Component, OnInit, ViewChild } from '@angular/core';
import {NgForm, FormControl, Validators} from '@angular/forms';
import { UserServiceService } from '../user-services/user-service.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  firstName: string;
  lastName: string;
  creditScore: number;
  annualIncome: number;

  creditScoreValidator: FormControl;
  annualIncomeValidator: FormControl;

  @ViewChild('userDetails') userDetails = new FormControl();

  constructor(public userService?: UserServiceService) {
    this.firstName = '';
    this.lastName = '';
    this.creditScore = 0;
    this.annualIncome = 0;
   }

  ngOnInit() {
    this.creditScoreValidator = new FormControl(1, [Validators.max(850), Validators.min(1)] );
    this.annualIncomeValidator = new FormControl(1, [Validators.min(1)]);
  }

  userDidEnterDetails(userDetailsssForm: NgForm) {
    console.log('user did enter details function got checked in');
    console.log(userDetailsssForm);
    this.userService.getUserProductOptions(userDetailsssForm);
  }

}
